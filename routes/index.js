const router = require('express').Router();

const properties = require('./properties');

router.use('/properties', properties);

module.exports = router;
