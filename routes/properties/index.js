const router = require('express').Router();

const { find, create, update, remove } = require('../../service/properties');

/**
 * Fetch all properties.
 */
router.get('/', (req, res) => {
  find((error, result) => {
    if (error) {
      res.status(500).send({ error });
    } else {
      res.status(200).send(result);
    }
  });
});

/**
 * Create new property
 */
router.post('/', (req, res) => {
  create(req.body, (error) => {
    if (error) {
      res.status(500).send({ error });
    } else {
      res.status(201).send();
    }
  });
});

/**
 * Update a property.
 */
router.put('/', (req, res) => {
  update(req.body, (error, statusCode) => {
    if (error) {
      res.status(statusCode).send({ error });
    } else {
      res.status(statusCode).send();
    }
  })
});

/**
 * Delete a property from its airbnbId
 */
router.delete('/:airbnbId', (req, res) => {
  remove(req.params.airbnbId, (error, statusCode) => {
    if (error) {
      res.status(statusCode).send({ error });
    } else {
      res.status(statusCode).send();
    }
  });
});

module.exports = router;
