create table properties (
  owner character varying(255) not null,
  address json not null,
  numberOfBedrooms smallint not null,
  numberOfBathrooms smallint not null,
  airbnbId bigint not null primary key,
  incomeGenerated numeric(10,2) not null
);

create table properties_history (
  action character varying(16) not null,
  timestamp timestamp not null,
  owner character varying(255),
  address json,
  numberOfBedrooms smallint,
  numberOfBathrooms smallint,
  airbnbId bigint not null,
  incomeGenerated numeric(10,2)
);

-- NOT creating a constraint so that the audit trail is kept even if the property is deleted
--alter table properties_history add constraint fk_properties_airbnbId foreign key (airbnbId) references properties (airbnbId);
