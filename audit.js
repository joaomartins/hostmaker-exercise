const eventHandler = require('./service/event-handler');

const dbClient = require('./db-client');

const query = 'INSERT INTO properties_history (\
                      action, \
                      timestamp, \
                      owner, \
                      address, \
                      numberofbedrooms, \
                      numberofbathrooms, \
                      airbnbid, \
                      incomegenerated) \
                      VALUES ($1, NOW(), $2, $3, $4, $5, $6, $7)';

const insertHistory = (action, property) => {
  dbClient.query(query, [
    action,
    property.owner, 
    property.address, 
    property.numberOfBedrooms, 
    property.numberOfBathrooms, 
    property.airbnbId, 
    property.incomeGenerated
  ]);
};

eventHandler.addListener('createProperty', property => {
  insertHistory('CREATE', property);
});

eventHandler.addListener('updateProperty', property => {
  insertHistory('UPDATE', property);
});

eventHandler.addListener('removeProperty', property => {
  insertHistory('REMOVE', property);
});
