# README #

Backend development exercise for Hostmaker.

## API description ##

## GET /api/properties
List existing properties.

## POST /api/properties
Create a new property.

## PUT /api/properties
Update an existing property.

## DELETE /api/properties/<id>
Deletes the property with airbnbId <id>