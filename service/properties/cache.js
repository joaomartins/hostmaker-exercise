/**
 * Could be externalized to the database (as it doesn't change that much)
 * or to an in-memory cache (e.g. redis)
 */
const cache = [];

/**
 * Checks the in-memory cache for an already-checked valid airbnbId.
 * 
 * @param {Number} airbnbId 
 * @param {Function} callback
 */
const exists = (airbnbId, callback) => {
  callback(cache.includes(airbnbId));
};

/**
 * Adds an valid checked-for airbnbId to the cache.
 * 
 * @param {Number} airbnbId 
 */
const add = airbnbId => {
  cache.push(airbnbId);
};

module.exports = { add, exists };
