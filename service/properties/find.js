const dbClient = require('../../db-client');

const query = 'SELECT owner, \
                      address, \
                      numberofbedrooms, \
                      numberofbathrooms, \
                      airbnbid, \
                      incomegenerated \
                 FROM properties';

/**
 * Fetch all properties.
 * 
 * @param {Function} callback 
 */
const find = callback => {
  dbClient.query(query)
    .then(result => callback(null, result.rows))
    .catch(err => callback(err.detail, null));
};

module.exports = find;
