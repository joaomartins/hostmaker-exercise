const eventHandler = require('../event-handler');

const dbClient = require('../../db-client');

const query = 'DELETE FROM properties \
                     WHERE airbnbid = $1';

/**
 * Delete a property from its airbnbId.
 * 
 * @param {Number} airbnbId
 * @param {Function} callback 
 */
const remove = (airbnbId, callback) => {
  const args = [ airbnbId ];
  
  dbClient.query(query, args)
    .then(result => {
      if (result.rowCount === 0) {
        callback('Could not find such entity.', 404);
      } else {
        callback(null, 204)
        eventHandler.emit('removeProperty', { airbnbId });
      }
    }).catch(err => callback(err.detail, 500));
};

module.exports = remove;
