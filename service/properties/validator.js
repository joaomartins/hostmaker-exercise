const request = require('request');

const cache = require('./cache');

/**
 * Extracts the property data from a request's body and returns it, if valid.
 * 
 * @param {Object} body 
 * @param {Function} callback 
 */
const parsePropertyFromBody = (body, callback) => {
    const { owner, 
      address, 
      numberOfBedrooms, 
      numberOfBathrooms, 
      airbnbId, 
      incomeGenerated } = body;
    
    const property = { owner, 
      address, 
      numberOfBedrooms, 
      numberOfBathrooms, 
      airbnbId, 
      incomeGenerated };
  
    isValidPropertyData(property, (error, isValid) => {
      if (isValid) {
        callback(null, property);
        
      } else {
        callback('Invalid data provided.', null);
      }
    });
  };
  
  /**
   * Validates a property data and Airbnb ID
   * 
   * @param {Object} property
   * @param {Function} callback
   */
  const isValidPropertyData = (property, callback) => {
    const airbnbUrl = 'https://www.airbnb.co.uk/rooms/';
    const validProperty = property.owner 
      && property.address.line1 
      && property.address.line4 
      && property.address.postCode 
      && property.address.city 
      && property.address.country
      && property.numberOfBedrooms >= 0
      && property.numberOfBathrooms > 0
      && property.incomeGenerated > 0;
    
    if (!validProperty) {
      callback('Invalid data provided.', false);
  
    } else {
      cache.exists(property.airbnbId, exists => {
        if (exists) {
          callback(null, true);
          
        } else {
          request({
            url: `${airbnbUrl}${property.airbnbId}`,
            headers: { 'User-Agent': 'request' }
          },
          (error, response, body) => {
            cache.add(property.airbnbId);
            callback(error, response.statusCode === 200 && validProperty);
          });
        }
      });
    }
  };
  
  module.exports = {
      parsePropertyFromBody, 
      isValidPropertyData 
    };
