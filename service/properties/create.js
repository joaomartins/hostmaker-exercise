const eventHandler = require('../event-handler');

const dbClient = require('../../db-client');
const { parsePropertyFromBody } = require('./validator');

const query = 'INSERT INTO properties (\
                      owner, \
                      address, \
                      numberofbedrooms, \
                      numberofbathrooms, \
                      airbnbid, \
                      incomegenerated) \
               VALUES ($1, $2, $3, $4, $5, $6)';

/**
 * Create new property.
 * 
 * @param {Object} body
 * @param {Function} callback 
 */
const create = (body, callback) => {
  parsePropertyFromBody(body, (error, property) => {
    if (error) {
      callback(error, 400);
    } else {
      const args = [ property.owner, property.address, property.numberOfBedrooms, property.numberOfBathrooms, property.airbnbId, property.incomeGenerated ];
      
      dbClient.query(query, args)
        .then(result => {
          callback(null, 201);
          eventHandler.emit('createProperty', property);
        })
        .catch(err => callback(err.detail, 500));
    }
  });
};

module.exports = create;
