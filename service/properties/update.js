const eventHandler = require('../event-handler');

const dbClient = require('../../db-client');
const { parsePropertyFromBody } = require('./validator');

const query = 'UPDATE properties \
                  SET owner = $2, \
                      address = $3, \
                      numberofbedrooms = $4, \
                      numberofbathrooms = $5, \
                      incomegenerated = $6 \
                WHERE airbnbid = $1';

/**
 * Update a property.
 * 
 * @param {Object} body
 * @param {Function} callback 
 */
const update = (body, callback) => {
  parsePropertyFromBody(body, (error, property) => {
    if (error) {
      callback(error, 400);
    } else {
      const args = [ property.airbnbId, property.owner, property.address, property.numberOfBedrooms, property.numberOfBathrooms, property.incomeGenerated ];
      
      dbClient.query(query, args)
        .then(result => {
          if (result.rowCount === 0) {
            callback('Could not find such entity.', 404);
          } else {
            callback(null, 204);
            eventHandler.emit('updateProperty', property);
          }
        }).catch(err => callback(err.message, 500));
    }
  });
};

module.exports = update;
