const find = require('./find');
const create = require('./create');
const update = require('./update');
const remove = require('./remove');

module.exports = {
    find,
    create,
    update,
    remove
};
