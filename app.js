const express = require('express');
const bodyParser = require('body-parser');

const routes = require('./routes');

require('./audit');

const port = process.env.PORT || 3000;

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api', routes);

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
